<?php

require_once dirname(__FILE__) . '/lexical_analyzer/lexical_analyzer.php';
require_once dirname(__FILE__) . '/env/env.php';
require_once dirname(__FILE__) . '/stmts/stmts.php';

while (true) {
    echo 'Enter your script filename : ';

    $filename = trim(fgets(STDIN));

    if (file_exists($filename)) break;

    echo "No such file or directory (\"$filename\"). ";
    echo "Press Ctrl+C if you want to exit.\n";
}

try {
    $scripts = file($filename);
} catch (Exception $e) {
    exit ("Failed to open \"$filename\".");
}

$la = new LexicalAnalyzer($scripts);
$env = new Env();
$envs = [$env];

while (true) {
    $stmts = Stmts::is_match($la, $envs);
    if(!$stmts->parse()) exit('Invalid syntax.' . "\n");
    $stmts->execute();

    $unit = $la->get();
    if ($unit->get_type() === LexicalType::EOF) break;
    $la->unget($unit);
}
