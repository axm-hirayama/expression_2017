<?php

require_once dirname(__FILE__) . '/../node/node.php';
require_once dirname(__FILE__) . '/../node/expr.php';
require_once dirname(__FILE__) . '/../node/str.php';
require_once dirname(__FILE__) . '/../stmt/stmt_asgmt.php';
require_once dirname(__FILE__) . '/../stmt/stmt_print.php';
require_once dirname(__FILE__) . '/../stmt/stmt_for.php';

class Stmts
{
    private $_la;
    private $_envs;

    private $_stack = [];
    private $_type;

    public function __construct(LexicalAnalyzer $la, array &$envs, $type)
    {
        $this->_la   = $la;
        $this->_envs = $envs;
        $this->_type = $type;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs, $type = 'program')
    {
        return new Stmts($la, $envs, $type); // $typeで分けるとかできるかなとか
    }

    public function parse(): bool
    {
        while (true) {
            $unit = $this->_la->get();
            if ($unit->get_type() === LexicalType::EOL) continue;
            if ($unit->get_type() === LexicalType::EOF) break;
            if ($this->_is_end($unit)) break;
            $this->_la->unget($unit);

            // 数式，文字列（何もしない）
            $expr = Expr::is_match($this->_la, $this->_envs)
                    ?? Str::is_match($this->_la, $this->_envs);
            if ($expr !== null and $expr->parse()) continue;

            // 代入文
            $stmt = StmtAsgmt::is_match($this->_la, $this->_envs);
            if ($stmt !== null and $stmt->parse()) {
                if ($this->_la->get()->get_type() !== LexicalType::EOL) return false;
                $this->_stack[] = $stmt;
                $stmt->execute();
                continue;
            }

            // 他の文
            $stmt = StmtPrint::is_match($this->_la, $this->_envs)
                    ?? StmtFor::is_match($this->_la, $this->_envs);

            if ($stmt !== null and $stmt->parse()) {
                if ($this->_la->get()->get_type() !== LexicalType::EOL) return false;
                $this->_stack[] = $stmt;
                continue;
            }

            return false;
        }
        return true;
    }

    public function execute()
    {
        while ($this->_stack !== []) {
            array_shift($this->_stack)->execute();
        }
    }

    private function _is_end($unit): bool
    {
        // $typeごとに終了のキーワードを設定
        if ($unit->get_type() === LexicalType::EOF) return true;
        if ($this->_type === 'for' and $unit->get_value() === 'done') return true;
        return false;
    }
}
