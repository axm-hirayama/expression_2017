<?php

function get_var($name, array $envs)
{
    while ($envs !== []) {
        $env = array_pop($envs);
        if ($env->$name === null) continue;
        $value = $env->$name;
        break;
    }
    return $value ?? null;
}

function has_var($name, array $envs): bool
{
    return get_var($name, $envs) !== null;
}
