<?php

class Env
{
    private $_vars = [];

    public function __get($name)
    {
        return $this->_vars[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->_vars[$name] = $value;
        return $this;
    }
}
