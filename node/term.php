<?php

require_once dirname(__FILE__) . '/node.php';

class Term extends Node
{
    private $_left;
    private $_right;
    private $_operation;

    public function __construct(Node $left, Node $right, LexicalUnit $operation)
    {
        $this->_left      = $left;
        $this->_right     = $right;
        $this->_operation = $operation;
    }

    public function get_value()
    {
        $left_value  = $this->_left->get_value();
        $right_value = $this->_right->get_value();

        return [
            LexicalType::MUL => $left_value * $right_value,
            LexicalType::DIV => $left_value / $right_value
        ][$this->_operation->get_type()]; // int
    }
}
