<?php

require_once dirname(__FILE__) . '/node.php';
require_once dirname(__FILE__) . '/term.php';
require_once dirname(__FILE__) . '/num.php';

class Expr extends Node
{
    private $_la;
    private $_envs;

	private $_operand_stack   = [];
	private $_operation_stack = [];

    public function __construct(LexicalAnalyzer $la, array &$envs)
    {
        $this->_la = $la;
        $this->_envs = $envs;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        $unit = $la->get();
        $la->unget($unit);
        if (in_array($unit->get_type(), [LexicalType::INTEGER, LexicalType::FLOAT])) {
            return new Expr($la, $envs);
        }
        return null;
    }

    public function parse(): bool
    {
        while (true) {
            $unit = $this->_la->get();

            if (in_array($unit->get_type(), [LexicalType::INTEGER, LexicalType::FLOAT])) {
                $this->_operand_stack[] = new Num($unit->get_value(), $unit->get_type());
            } elseif ($unit->get_type() === LexicalType::ADD) {
                $this->_operation_stack[] = $unit;
            } elseif ($unit->get_type() === LexicalType::SUB) {
                $this->_operation_stack[] = $unit;
            } elseif (in_array($unit->get_type(), [LexicalType::MUL, LexicalType::DIV])) {
                $left  = array_pop($this->_operand_stack);

                $right_unit = $this->_la->get();
                $right = new Num($right_unit->get_value(), $right_unit->get_type());

                $this->_operand_stack[] = new Term($left, $right, $unit);
            } elseif (in_array($unit->get_type(),
                               [LexicalType::EOL, LexicalType::EOF, LexicalType::RESERVED_WORD])) {
                $this->_la->unget($unit);
                return true;
            } else {
                return false;
            }
        }
    }

    public function get_value()
    {
        while ($this->_operation_stack !== []) {
            $operation = array_shift($this->_operation_stack);
            $left      = array_shift($this->_operand_stack)->get_value();
            $right     = array_shift($this->_operand_stack)->get_value();

            $result = [
                LexicalType::ADD => $left + $right,
                LexicalType::SUB => $left - $right
            ][$operation->get_type()];

            array_unshift($this->_operand_stack, new Num($result));
        }

        return array_shift($this->_operand_stack);
    }
}
