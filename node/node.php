<?php

require_once dirname(__FILE__) . '/../lexical_analyzer/lexical_analyzer.php';
require_once dirname(__FILE__) . '/../env/env.php';

class Node
{
    public function __construct()
    {
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        exit ('Called function, is_match() is not defined in this class.');
    }

    public function parse(): bool
    {
        exit ('Called function, parse() is not defined in this class.');
    }

    public function get_value()
    {
        exit ('Called function, get_value() is not defined in this class.');
    }
}
