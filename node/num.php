<?php

require_once dirname(__FILE__) . '/node.php';

class Num extends Node
{
    private $_type;
    private $_value;

    public function __construct($value, $type = LexicalType::FLOAT)
    {
        $this->_value = $value;
        $this->_type  = $type;
    }

    public function get_value()
    {
        return [
            LexicalType::INTEGER => (int)$this->_value,
            LexicalType::FLOAT   => (float)$this->_value
        ][$this->_type];
    }
}
