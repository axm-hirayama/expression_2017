<?php

require_once dirname(__FILE__) . '/node.php';

class Variable extends Node
{
    private $_name;
    private $_envs;

    public function __construct($name, array &$envs)
    {
        $this->_name = $name;
        $this->_envs = $envs;
    }

    function set_value($value)
    {
        if (is_float($value))  $value = new Num($value, LexicalType::FLOAT);
        if (is_int($value))    $value = new Num($value, LexicalType::INTEGER);
        if (is_string($value)) $value = new LexicalUnit(LexicalType::STRING, $value);

        $name = $this->_name;
        $env = array_pop($this->_envs);
        $env->$name = $value;
        array_push($this->_envs, $env);
    }

    function get_value()
    {
        return get_var($this->_name, $this->_envs);
    }

}
