<?php

require_once dirname(__FILE__) . '/node.php';

class Str extends Node
{
    private $_la;
    private $_envs;

    private $_string;

    public function __construct(LexicalAnalyzer $la, array &$envs)
    {
        $this->_la = $la;
        $this->_envs = $envs;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        $unit = $la->get();
        if ($unit->get_type() === LexicalType::SINGLE_QUOTATION) {
            return new Str($la, $envs);
        }
        $la->unget($unit);
        return null;
    }

    public function parse(): bool
    {
        $this->_string = $this->_la->get($is_str = true);
        return ($this->_string->get_type() === LexicalType::STRING);
    }

    public function get_value()
    {
        return $this->_string; // LexicalType::STRING
    }
}
