<?php
require_once dirname(__FILE__) . '/lexical_type.php';

class LexicalUnit
{
	private $_type;
	private $_value;

	public function __construct($type, $value)
	{
		$this->_type = $type;
		$this->_value = $value;
	}

	public function get_type(): String
	{
		return $this->_type;
	}

	public function get_value()
	{
		return $this->_value;
	}

}
