<?php

class Reserved
{
    const OPERATIONS = [
        '+',
        '-',
        '*',
        '/',
        '=',
    ];

    const WORDS = [
        'print',
        'for',
        'to',
        'step',
        'done',
    ];

    const REGEXPS = [
        'n' => PHP_EOL,
        '\'' => '\'',
        '\\' => '\\',
    ];
}
