<?php

require_once dirname(__FILE__) . '/lexical_type.php';
require_once dirname(__FILE__) . '/lexical_unit.php';
require_once dirname(__FILE__) . '/reserved.php';

class LexicalAnalyzer
{
    private $_scripts;
    private $_line  = 0;
    private $_index = -1;
    private $_stack = [];

    public function __construct(array $scripts)
    {
        $this->_scripts = $scripts;
    }

    public function get(bool $is_str = false): LexicalUnit
    {
        // unget()の後
        if (!empty($this->_stack)) {
            return array_pop($this->_stack);
        }

        // ファイル終端
        if ($this->_line >= count($this->_scripts)) {
            return new LexicalUnit(LexicalType::EOF, null);
        }

        $script = $this->_scripts[$this->_line];

        // シングルクオート内
        if ($is_str) {
            $str = '';
            while (true) {
                $this->_index++;

                if ($this->_line >= count($this->_scripts) or
                    $script[$this->_index] === PHP_EOL) {
                    return new LexicalUnit(LexicalType::ERROR, null);
                }

                if ($script[$this->_index] === '\'') break;

                if ($script[$this->_index] === '\\') {
                    $this->_index++;
                    $str .= $this->_convert_regexp($script[$this->_index]);
                    continue;
                }

                $str .= $script[$this->_index];
            }
            return new LexicalUnit(LexicalType::STRING, $str);
        }

        // シングルクオート外
        while (true) {
            $this->_index++;

            // 行末，コメントアウト
            if ($script[$this->_index] === PHP_EOL or
                $this->_is_comment($script[$this->_index])) {
                $this->_line++;
                $this->_index = -1;
                return new LexicalUnit(LexicalType::EOL, null);
            }

            // スペース，タブ
            if (in_array($script[$this->_index], [' ', '\t'])) {
                continue;
            }

            // シングルクオート
            if ($script[$this->_index] === '\'') {
                return new LexicalUnit(LexicalType::SINGLE_QUOTATION,
                                       $script[$this->_index]);
            }

            // 演算子
            if ($this->_is_operator($script[$this->_index])) {
                $type = [
                    '+' => LexicalType::ADD,
                    '-' => LexicalType::SUB,
                    '*' => LexicalType::MUL,
                    '/' => LexicalType::DIV,
                    '=' => LexicalType::EQUAL
                ][$script[$this->_index]];

                return new LexicalUnit($type, $script[$this->_index]);
            }

            // 数字
            if (is_numeric($script[$this->_index])) {
                $number = $script[$this->_index];

                while (true) {
                    $this->_index++;

                    if (!$this->_is_num($script[$this->_index])) break;
                    $number .= $script[$this->_index];
                }

                $this->_index--;

                if (strpos($number, '.') === false) {
                    return new LexicalUnit(LexicalType::INTEGER, $number);
                }
                return new LexicalUnit(LexicalType::FLOAT, $number);
            }

            // 予約語・変数名
            if (ctype_alpha($script[$this->_index])) {
                $str = $script[$this->_index];

                while (true) {
                    $this->_index++;
                    if (!ctype_alnum($script[$this->_index])) break;
                    $str .= $script[$this->_index];
                }

                $this->_index--;

                if ($this->_is_reserved_word($str)) {
                    return new LexicalUnit(LexicalType::RESERVED_WORD, $str);
                }
                return new LexicalUnit(LexicalType::VAR_STRING, $str);
            }

            return new LexicalUnit(LexicalType::ERROR, $script[$this->_index]);
        }
    }

    public function unget(LexicalUnit $unit)
    {
        array_push($this->_stack, $unit);
        return $this;
    }

    private function _is_operator($val)
    {
        return in_array($val, Reserved::OPERATIONS);
    }

    private function _is_num($val)
    {
        return (is_numeric($val) or
                $val === '.');
    }

    private function _is_reserved_word($char)
    {
        return in_array($char, Reserved::WORDS);
    }

    private function _is_comment($val)
    {
        return in_array($val, ['#']);
    }

    private function _convert_regexp($char)
    {
        return Reserved::REGEXPS[$char];
    }
}
