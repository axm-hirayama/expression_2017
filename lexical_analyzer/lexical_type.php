<?php

class LexicalType
{
	const INTEGER = 'INT';
    const FLOAT   = 'FLOAT';

	const ADD = 'ADD';
	const SUB = 'SUB';
	const MUL = 'MUL';
	const DIV = 'DIV';

    const EQUAL = 'EQ';

    const STRING   = 'STR';
    const VAR_STRING = 'V_STR';

    const SINGLE_QUOTATION = 'SQUOTE';

    const RESERVED_WORD = 'RW';

	const EOF = 'EOF';
    const EOL = 'EOL';

	const ERROR = 'ERR';
}
