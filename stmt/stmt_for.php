<?php

require_once dirname(__FILE__) . '/stmt.php';
require_once dirname(__FILE__) . '/stmt_asgmt.php';
require_once dirname(__FILE__) . '/stmt_print.php';

/*
for i=1 to 10 [step 2] do
    ...
done
*/

class StmtFor extends Stmt
{
    private $_la;
    private $_envs;

    private $_env;
    private $_stmts;        // ... Stmts
    private $_init_counter; // i=1 StmtAsgmt
    private $_end;          // 10  int
    private $_step = 1;     // 2   int

    public function __construct(LexicalAnalyzer $la, array &$envs)
    {
        $this->_la   = $la;

        $this->_env  = new Env();

        $this->_envs   = $envs;
        $this->_envs[] = $this->_env;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        $unit = $la->get();
        if ($unit->get_value() === 'for') {
            return new StmtFor($la, $envs);
        }
        $la->unget($unit);
        return null;
    }

    public function parse(): bool
    {
        // i=1
        $this->_init_counter = StmtAsgmt::is_match($this->_la, $this->_envs);
        if ($this->_init_counter === null or !$this->_init_counter->parse()) {
            return false;
        }
        $this->_init_counter->execute();

        // to
        if ($this->_la->get()->get_value() !== 'to') return false;

        // 10
        $end = $this->_la->get();
        if ($end->get_type() !== LexicalType::INTEGER) return false;
        $this->_end = new Num($end->get_value(), LexicalType::INTEGER);
        $this->_end = $this->_end->get_value();

        $next = $this->_la->get();
        // step
        if ($next->get_value() === 'step') {
            $step = $this->_la->get();
            if ($step->get_type() !== LexicalType::INTEGER) return false;
            $this->_step = new Num($step->get_value(), LexicalType::INTEGER);
            $this->_step = $this->_step->get_value();

            $next = $this->_la->get();
        }

        // do
        if ($next->get_value() !== 'do') return false;
        if ($this->_la->get()->get_type() !== LexicalType::EOL) return false;

        // ... done
        $this->_stmts = Stmts::is_match($this->_la, $this->_envs, $type = 'for');
        if(!$this->_stmts->parse()) return false;

        return true;
    }

    public function execute()
    {
        $counter = new Variable($this->_init_counter->execute(), $this->_envs);

        while ($counter->get_value()->get_value() <= $this->_end) {
            $stmts = clone $this->_stmts;
            $stmts->execute();

            $counter->set_value($counter->get_value()->get_value() + $this->_step);
        }
    }
}
