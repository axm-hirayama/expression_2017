<?php

require_once dirname(__FILE__) . '/../lexical_analyzer/lexical_analyzer.php';
require_once dirname(__FILE__) . '/../env/env.php';
require_once dirname(__FILE__) . '/../env/functions.php';
require_once dirname(__FILE__) . '/../node/expr.php';
require_once dirname(__FILE__) . '/../node/str.php';
require_once dirname(__FILE__) . '/../node/variable.php';

class Stmt
{
    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        exit ('Called function, is_match() is not defined in this class.');
    }

    public function parse(): bool
    {
        exit ('Called function, parse() is not defined in this class.');
    }

    public function execute()
    {
        exit ('Called function, execute() is not defined in this class.');
    }
}
