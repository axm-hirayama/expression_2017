<?php

require_once dirname(__FILE__) . '/stmt.php';

class StmtPrint extends Stmt
{
    private $_la;
    private $_envs;

    private $_object;

    public function __construct(LexicalAnalyzer $la, array &$envs)
    {
        $this->_la   = $la;
        $this->_envs = $envs;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        $unit = $la->get();
        if ($unit->get_value() === 'print') return new StmtPrint($la, $envs);

        $la->unget($unit);
        return null;
    }

    public function parse(): bool
    {
        $right = $this->_la->get();
        // 右辺が変数
        if ($right->get_type() === LexicalType::VAR_STRING) {
            $this->_object  = new Variable($right->get_value(), $this->_envs);
            return has_var($right->get_value(), $this->_envs);
        }

        $this->_la->unget($right);

        // 右辺が数式または文字列
        $right = Expr::is_match($this->_la, $this->_envs)
                 ?? Str::is_match($this->_la, $this->_envs);
        if ($right !== null and $right->parse()) {
            $this->_object = $right;
            return true;
        }
        return false;
    }

    public function execute()
    {
        // Variable -> Num / STR -> int / string
        // Expr     -> Num       -> int
        // Str      -> STR       -> string
        echo $this->_object->get_value()->get_value();
    }
}
