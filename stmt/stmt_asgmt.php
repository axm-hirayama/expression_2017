<?php

require_once dirname(__FILE__) . '/stmt.php';

class StmtAsgmt extends Stmt
{
    private $_la;
    private $_envs;

    private $_name;
    private $_value;

    public function __construct(LexicalAnalyzer $la, array &$envs)
    {
        $this->_la   = $la;
        $this->_envs = $envs;
    }

    public static function is_match(LexicalAnalyzer $la, array &$envs)
    {
        $units = [
            'left'      => $la->get(),
            'operation' => $la->get()
        ];

        $la->unget($units['operation'])->unget($units['left']);

        if ($units['left']->get_type()      === LexicalType::VAR_STRING and
            $units['operation']->get_type() === LexicalType::EQUAL) {
                return new StmtAsgmt($la, $envs);
        }
        return null;
    }

    public function parse(): bool
    {
        $this->_name = $this->_la->get()->get_value(); // string

        $operation = $this->_la->get(); // EQ
        $right     = $this->_la->get(); // 変数名，Expr，Strのはず

        // 右辺が変数
        if ($right->get_type() === LexicalType::VAR_STRING) {
            $this->_value = new Variable($right->get_value(), $this->_envs);
            return has_var($right->get_value(), $this->_envs);
        }

        // 右辺が数式または文字列
        $this->_la->unget($right);
        $right = Expr::is_match($this->_la, $this->_envs)
                 ?? Str::is_match($this->_la, $this->_envs);
        if ($right === null) return false;
        if (!$right->parse()) return false;
        $this->_value = $right;
        return true;
    }

    public function execute()
    {
        $var   = new Variable($this->_name, $this->_envs);
        $value = clone $this->_value;

        $var->set_value($value->get_value());
        return $this->_name;
    }
}
