# 2017春休みの課題

## 前回：四則演算
Backlog：[10PER-88](https://axm-sysdev.backlog.jp/view/10PER-88)
Bitbucket：[expression](https://bitbucket.org/axm-hirayama/expression)

## 今回：簡易インタプリタ
Backlog：[10PER-257](https://axm-sysdev.backlog.jp/view/10PER-257)
Bitbucket：[expression_2017](https://bitbucket.org/axm-hirayama/expression_2017)

## 使い方
- `php main.php` を実行するとファイル名の入力を求められる → 拡張子含めて手打ち
- サンプルは `script_1.txt` と `script_2.txt`
- script_1.txt：演算，代入，print，forの基本的な挙動
- script_2.txt：for内で変数のスコープが切れているか

### 文法
- 代入文
```
a = 1
b = 2 * 3
c = 'あ'
d = c
```

- print文
```
print 1
print 2 * 3
print 'あ'
print a
```

- for文
```
for i = 1 to 10 [step 2] do
  (statements)
done
```

- コメントアウト
`a = 1 # aは1です`

- 文字列中の改行，エスケープ
    - 改行 `'改行します：\n'`
    - バックスラッシュ `'バックスラッシュです：\\'`
    - シングルクォーテーション `'シングルクォーテーションです：\''`

## 心残り（1/25）
- エラー処理が足りずコードの誤りに弱い
- 四則演算が括弧と負の数に対応していない
- 変数同士の演算に対応していない

- 文字列の扱いが下手
- 代入文の処理が下手（パース時・実行時で二度実行している）  
続く文で出てくる変数名が合っているかをparse()内で確かめるには一度envに書き込まないといけなかった
- 自分にしかわからないどころか自分にもわからない闇のコード